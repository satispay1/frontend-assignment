import { pipe } from "fp-ts/lib/pipeable";
import * as O from "fp-ts/lib/Option";
import * as A from "fp-ts/lib/Array";
import { identity } from "fp-ts/lib/function";
import { data } from "../data/pokemons";
import { toConnection, slice } from "../functions";
import { Connection } from "../types";

interface Pokemon {
  id: string;
  name: string;
  types: string[];
  classification: string;
}

const SIZE = 10;

// Function factory to be DRY and use it with both functions
const filterByQFunction = (q: string | undefined) => {
  // filter only if q is defined
  if (q === undefined) return identity;

  const filterByQ: (as: Pokemon[]) => Pokemon[] = A.filter((p) =>
    p.name.toLowerCase().includes(q.toLowerCase())
  );
  return filterByQ;
};

// Function factory to be DRY and use it with both functions
const filterByTypeFunction = (type: string | undefined) => {
  // filter type if type is defined
  if (type === undefined) return identity;

  const filterByType: (as: Pokemon[]) => Pokemon[] = A.filter((p) =>
    p.types.some((t) => t === type)
  );
  return filterByType;
};

// Function factory to be DRY and use it with both functions
const sliceByAfterFunction = (after: string | undefined) => {
  // filter only if after is defined
  if (after === undefined) return identity;

  const sliceByAfter: (as: Pokemon[]) => Pokemon[] = (as) =>
    pipe(
      as,
      A.findIndex((a) => a.id === after),
      O.map((a) => a + 1),
      O.fold(
        () => as,
        (idx) => as.slice(idx)
      )
    );
  return sliceByAfter;
};

export function query(args: {
  after?: string;
  limit?: number;
  q?: string;
  type?: string;
}): Connection<Pokemon> {
  const { after, q, type, limit = SIZE } = args;

  const filterByQ = filterByQFunction(q);

  const filterByType = filterByTypeFunction(type);

  const sliceByAfter = sliceByAfterFunction(after);

  const results: Pokemon[] = pipe(
    data,
    filterByQ,
    filterByType,
    sliceByAfter,
    // slicing limit + 1 because the `toConnection` function should known the connection size to determine if there are more results
    slice(0, limit + 1)
  );
  return toConnection(results, limit);
}

export function queryByType(args: {
  after?: string;
  limit?: number;
  type: string;
}): Connection<Pokemon> {
  const { after, type, limit = SIZE } = args;

  const filterByType = filterByTypeFunction(type);

  const sliceByAfter = sliceByAfterFunction(after);

  const results: Pokemon[] = pipe(
    data,
    filterByType,
    sliceByAfter,
    slice(0, limit + 1)
  );
  return toConnection(results, limit);
}

export function types(): string[] {
  const typesArray = data.map((p) => p.types).flat();
  const typesSet = new Set(typesArray);
  const typesSetAsArray = Array.from(typesSet);
  return typesSetAsArray;
}
