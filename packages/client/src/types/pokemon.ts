// NOTE beeing in a monorepo we could have used the backend types directly
// This would avoid code repetition but could cause problems if the project are not
//  correctly syncronized

type ID = string;

interface PageInfo {
  endCursor: ID;
  hasNextPage: boolean;
}

interface PokemonEdge {
  cursor: ID;
  node: Pokemon;
}

export interface PokemonResponse {
  pokemons: {
    edges: PokemonEdge[];
    pageInfo: PageInfo;
  };
}

export interface Pokemon {
  id: ID;
  name: string;
  types: string[];
  classification: string;
}

export type PokemonTypes = Pokemon["types"];

export type PokemonTypesResponse = {
  pokemonsTypes: PokemonTypes;
};
