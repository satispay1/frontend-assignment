import {
  SearchOutlined,
  DownloadOutlined,
  GitlabOutlined,
} from "@ant-design/icons";
import { useQuery } from "@apollo/client";
import {
  Button,
  Input,
  PageHeader,
  Space,
  Table,
  TablePaginationConfig,
} from "antd";
import "antd/dist/antd.css";
import { ColumnsType, ColumnType, FilterValue } from "antd/lib/table/interface";
import React, { useEffect, useMemo, useRef, useState } from "react";
import {
  Pokemon,
  PokemonResponse,
  PokemonTypes,
  PokemonTypesResponse,
} from "./types/pokemon";
import { POKEMONS, POKEMONS_TYPES } from "./api/backend";

function App() {
  // Internal filters status
  const [nameFilter, setNameFilter] = useState<string>(undefined);
  const [typeFilter, setTypeFilter] = useState<string>(undefined);

  const { data, loading, fetchMore, refetch } = useQuery<PokemonResponse>(
    POKEMONS,
    {
      variables: {
        pokemonName: nameFilter,
        pokemonType: typeFilter,
      },
      // Enable loading status update on refetch and fetchmore calls
      notifyOnNetworkStatusChange: true,
    }
  );

  // Handle data refetch on filter state change
  useEffect(() => {
    refetch({
      pokemonType: typeFilter,
      pokemonName: nameFilter,
    });
  }, [nameFilter, typeFilter]);

  // Extract data from relay data structure
  const nodes = useMemo(() => data?.pokemons.edges.map((edge) => edge.node), [
    data,
  ]);
  const pageInfo = data?.pokemons.pageInfo;

  // Possibile pokemon type fetched from backend and used for filtering
  const { data: pokemonTypesResponse } = useQuery<PokemonTypesResponse>(
    POKEMONS_TYPES
  );
  const pokemonsTypes = pokemonTypesResponse?.pokemonsTypes ?? [];

  // Handle the "Load More" button
  const handleButtonClick = () => {
    if (pageInfo?.hasNextPage) {
      fetchMore({ variables: { cursor: pageInfo.endCursor } });
    }
  };

  // Handle table filters update
  const handleTableChange = (
    _pagination: TablePaginationConfig,
    filters: Record<string, FilterValue>
  ) => {
    setTypeFilter(filters?.types?.[0].toString());
  };
  const handleSearch = (selectedKeys: any[], confirm: () => void) => {
    confirm();
    setNameFilter(selectedKeys[0]);
  };
  const handleReset = (selectedKeys: any[], clearFilters: () => void) => {
    clearFilters();
    selectedKeys.pop();
    setNameFilter(undefined);
  };

  // Ref for text input
  const inputRef = useRef(null);

  // Search props for table column config
  const searchProps: ColumnType<Pokemon> = {
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={inputRef}
          placeholder={`Search Pokemon Name`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => handleSearch(selectedKeys, confirm)}
          style={{ marginBottom: 8, display: "block" }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearch(selectedKeys, confirm)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Search
          </Button>
          <Button
            onClick={() => handleReset(selectedKeys, clearFilters)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
          </Button>
        </Space>
      </div>
    ),
    filterIcon: <SearchOutlined />,
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        // Reset focus to the text input
        setTimeout(() => inputRef.current.select(), 100);
      }
    },
  };

  const columns: ColumnsType<Pokemon> = [
    {
      title: "Name",
      dataIndex: "name",
      ...searchProps,
    },
    {
      title: "Type",
      dataIndex: "types",
      render: (types: PokemonTypes) => types.toString(),
      filters: pokemonsTypes?.map((type) => ({
        text: type,
        value: type,
      })),
      filterMultiple: false,
      filterSearch: true,
    },
    {
      title: "Classification",
      dataIndex: "classification",
    },
  ];

  return (
    <div>
      <PageHeader
        title="Pokedex"
        subTitle="Satispay Front End Assignment"
        tags={
          <Space>
            <a href="https://gitlab.com/satispay1/frontend-assignment">
              <GitlabOutlined /> Source Code
            </a>
          </Space>
        }
        style={{ backgroundColor: "#f5f5f5" }}
      />
      <Table
        columns={columns}
        rowKey={(record) => record.id}
        dataSource={nodes}
        loading={loading}
        pagination={false}
        scroll={{ y: 550 }}
        onChange={handleTableChange}
        footer={() => (
          <Button
            icon={<DownloadOutlined />}
            loading={loading}
            hidden={!pageInfo?.hasNextPage}
            onClick={handleButtonClick}
          >
            Load More
          </Button>
        )}
      />
    </div>
  );
}

export default App;
