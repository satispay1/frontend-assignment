import { gql } from "@apollo/client";

const POKEMON_MINIMAL_DATA = gql`
  fragment PokemonMinimalData on Pokemon {
    id
    name
    types
    classification
  }
`;

const PAGE_INFO = gql`
  fragment PageInfo on PageInfo {
    endCursor
    hasNextPage
  }
`;

export const POKEMONS = gql`
  ${POKEMON_MINIMAL_DATA}
  ${PAGE_INFO}
  query GetPokemons(
    $pokemonName: String
    $cursor: ID
    $limit: Int
    $pokemonType: String
  ) {
    pokemons(
      q: $pokemonName
      after: $cursor
      limit: $limit
      type: $pokemonType
    ) {
      edges {
        node {
          ...PokemonMinimalData
        }
      }
      pageInfo {
        ...PageInfo
      }
    }
  }
`;

export const POKEMONS_BY_TYPE = gql`
  ${POKEMON_MINIMAL_DATA}
  ${PAGE_INFO}

  query GetPokemonsByType($pokemonType: String) {
    pokemonsByType(type: $pokemonType) {
      edges {
        node {
          ...PokemonMinimalData
        }
      }
      pageInfo {
        ...PageInfo
      }
    }
  }
`;

export const POKEMONS_TYPES = gql`
  query GetPokemonsTypes {
    pokemonsTypes
  }
`;
